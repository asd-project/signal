//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/common.h>
#include <meta/concepts.h>

#include <meta/types.h>
#include <meta/tuple/apply.h>

#include <container/plain_tuple.h>
#include <container/function.h>

#include <tl/function_ref.hpp>

//---------------------------------------------------------------------------

namespace asd::signal
{
    struct connection;

    using send_function_t = void(*)(connection *, void * args);
    using reconnect_function_t = void(*)(connection *, connection *, void * source, void * destination);

    template <class F, class D>
    struct transmission
    {
        typename D::connection_type & connection;

    private:
        friend struct signal::connection;
        transmission(typename D::connection_type & c) : connection(c) {}
    };

    template <class D, class S>
    static constexpr bool destination_of = requires(D v, transmission<typename S::signal_type, D> & t, typename S::transmission_type args) {
        { meta::apply(v, std::forward_as_tuple(t), args) };
    };

    struct connection
    {
        // Target classes can grant access (via friendship) to their private members for the connection type
        // This concept-like predicates will use it to check hidden methods of those classes

        // Owning node manages connection lifetime
        // It is not required for a connection to have an owner, but usually there is an owner and it is a destination node
        // Connection should not have more than one owner
        template <class T>
        static constexpr bool owning_node = requires(T v, connection & c) {
            T::has_connection;
            T::connect;
            T::disconnect;
            T::cleanup_connection;

            requires derived_from<typename T::connection_type, connection>;
        };

        // Reference node doesn't own a connection (obviously), but gets notified when a connection is reset
        template <class T>
        static constexpr bool reference_node = requires(T v, connection & c) {
            { v.set_connection(c, &c) };
            { v.reset_connection(c) };
        };

        //

        connection() = default;

        template <class S, class D>
        connection(S & source, D & destination) {
            bind(source, destination);
        }

        connection(connection && c) noexcept :
            send(std::exchange(c.send, nullptr)),
            reconnect(std::exchange(c.reconnect, nullptr))
        {
            if (this->reconnect) {
                auto source = std::exchange(c.source, nullptr);
                auto destination = std::exchange(c.destination, nullptr);
                this->reconnect(&c, this, source, destination);
            }
        }

        ~connection() {
            if (this->reconnect) {
                this->reconnect(nullptr, this, nullptr, nullptr);
            }
        }

        connection & operator = (connection && c) noexcept {
            auto source = std::exchange(c.source, nullptr);
            auto destination = std::exchange(c.destination, nullptr);

            if (this->reconnect) {
                this->reconnect(nullptr, this, nullptr, nullptr);
            }

            this->send = std::exchange(c.send, nullptr);
            this->reconnect = std::exchange(c.reconnect, nullptr);

            if (this->reconnect) {
                this->reconnect(&c, this, source, destination);
            }

            return *this;
        }

        template <class S, class D>
        void bind(S & source, D & destination) {
            static_assert(node<S>, "Source is not valid signal node");
            static_assert(node<D>, "Destination is not valid signal node");
            static_assert(destination_of<D, S>, "Destination can not accept source signal");
            static_assert(!owning_node<S> || !owning_node<D>, "Source and destination can not both be owning nodes");

            this->send = connection::erased_send<S, D>;
            this->reconnect = connection::erased_reconnect<S, D>;

            connection::erased_reconnect<S, D>(nullptr, this, &source, &destination);
        }

        void * source = nullptr;
        void * destination = nullptr;
        send_function_t send = nullptr;
        reconnect_function_t reconnect = nullptr;

    private:
        template <class S, class D>
        static void erased_send(connection * self, void * args) {
            auto & d = *static_cast<D *>(self->destination);

            // temporary reset the destination to avoid subscription cleanup during the call
            self->destination = nullptr;

            signal::transmission<typename S::signal_type, D> transmission{static_cast<typename D::connection_type &>(*self)};

            meta::apply(d, std::forward_as_tuple(transmission), std::move(*static_cast<typename S::transmission_type *>(args)));

            self->destination = &d;

            // check if the source was disconnected during the call, can cleanup subscriptions now
            if (!self->source) {
                erased_reconnect<S, D>(nullptr, self, nullptr, nullptr);
            }
        }

        template <class S, class D>
        static void erased_reconnect(connection * previous, connection * self, void * source, void * destination) {
            update_node<S>(previous, self, self->source, source);
            update_node<D>(previous, self, self->destination, destination);
        }

        template <class T>
        static void update_node(connection * previous, connection * current, void *& current_node, void * new_node) {
            if constexpr (reference_node<T>) {
                if (current_node && !new_node) {
                    static_cast<T *>(current_node)->reset_connection(*current);
                    return;
                }

                if (new_node && !current_node) {
                    static_cast<T *>(new_node)->set_connection(*current, previous);
                    return;
                }
            } else if constexpr(owning_node<T>) {
                if (current_node && !new_node) {
                    static_cast<T *>(current_node)->cleanup_connection(static_cast<typename T::connection_type &>(*current));
                    return;
                }
            }

            current_node = new_node;
        }
    };

    template <class T>
    concept owning_node = connection::owning_node<T>;

    template <class T>
    concept reference_node = connection::reference_node<T>;

    template <class T>
    concept node = reference_node<T> || owning_node<T>;

    template <class T>
    concept source_node = node<T> && requires {
        typename T::signal_type;
        typename T::transmission_type;
    };

    template <class OutputIterator, class Result>
    concept result_output_iterator = requires {
        requires std::convertible_to<Result, typename std::iterator_traits<OutputIterator>::value_type>;
    } || requires {
        typename OutputIterator::container_type;
        typename OutputIterator::container_type::value_type;
        requires std::convertible_to<Result, typename OutputIterator::container_type::value_type>;
    };

    template <class OutputIterator, class Result>
    struct result_inserter
    {
        result_inserter(OutputIterator it) : it(it) {}

        void operator () (Result && value) {
            *it++ = std::forward<Result>(value);
        }

        OutputIterator it;
    };

    template <class R, class ... Arguments>
    struct signal_traits
    {
        using signal_type = R(Arguments...);
        using arguments_type = plain_tuple<Arguments...>;
        using result_type = R;
        using result_handler_type = tl::function_ref<void(R &&)>;
        using transmission_type = plain_tuple<result_handler_type &&, Arguments...>;
    };

    template <class ... Arguments>
    struct signal_traits<void, Arguments...>
    {
        using signal_type = void(Arguments...);
        using arguments_type = plain_tuple<Arguments...>;
        using result_type = void;
        using transmission_type = plain_tuple<Arguments...>;
    };
}
