//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/id.h>
#include <boost/container/stable_vector.hpp>

#include <signal/connection.h>

//---------------------------------------------------------------------------

namespace asd::signal
{
    using basic_subscription_id = meta::id<u32, struct basic_subscription_tag, std::integral_constant<u32, u32(-1)>>;

    template <class F>
    using subscription_id = meta::id<u32, F, std::integral_constant<u32, u32(-1)>>;

    using subscription_destroy_function_t = void (*)(void *);

    struct basic_subscription
    {
        basic_subscription_id id;
        u32 connection_id;
        subscription_destroy_function_t destroy;
    };

    template <class F>
    class subscription : public basic_subscription
    {
    public:
        subscription(u32 id, u32 connection_id, move_only_function<F> && handler) :
            basic_subscription{id, connection_id, subscription::destroy},
            handler(std::move(handler)) {}

        move_only_function<F> handler;

    private:
        static void destroy(void * ptr) {
            auto s = static_cast<subscription *>(ptr);
            s->~subscription();
        }
    };

    using basic_connection_id = meta::id<u32, struct basic_connection_tag, std::integral_constant<u32, u32(-1)>>;

    template <class F>
    using connection_id = meta::id<u32, F, std::integral_constant<u32, u32(-1)>>;

    struct hub_connection : connection
    {
        using allocator_type = std::pmr::polymorphic_allocator<hub_connection>;

        hub_connection(u32 id, const allocator_type & alloc = {}) noexcept :
            id(id),
            subscriptions(alloc)
        {}

        template <signal::source_node S, class D>
        hub_connection(S & source, D & destination, const allocator_type & alloc = {}) :
            connection(source, destination),
            subscriptions(alloc)
        {}

        hub_connection(hub_connection && c, const allocator_type & alloc = {}) noexcept :
            connection(std::move(c)),
            id(std::move(c.id)),
            subscriptions(std::move(c.subscriptions), alloc)
        {}

        hub_connection & operator = (hub_connection && c) noexcept = default;

        basic_connection_id id;
        std::pmr::vector<basic_subscription *> subscriptions;
    };

    /**
     * Type-erased subscriptions holder
     */
    class hub
    {
    public:
        using allocator_type = std::pmr::polymorphic_allocator<hub>;
        using connection_type = signal::hub_connection;

        hub(const allocator_type & alloc = {}) noexcept :
            _connections(alloc),
            _subscriptions(alloc),
            _connections_free_list(alloc),
            _subscriptions_free_list(alloc)
        {}

        hub(hub && h, const allocator_type & alloc = {}) noexcept :
            _connections(std::move(h._connections), alloc),
            _subscriptions(std::move(h._subscriptions), alloc),
            _connections_free_list(std::move(h._connections_free_list), alloc),
            _subscriptions_free_list(std::move(h._subscriptions_free_list), alloc)
        {
            update_connections();
        }

        ~hub() {
            for (auto & c : _connections) {
                if (c.reconnect != nullptr) {
                    c.reconnect(nullptr, &c, nullptr, c.destination);
                    c.destination = nullptr;
                }

                for (auto * s : c.subscriptions) {
                    s->destroy(s);
                }
            }
        }

        hub & operator = (hub && h) noexcept {
            _connections = std::move(h._connections);
            _subscriptions = std::move(h._subscriptions);
            _connections_free_list = std::move(h._connections_free_list);
            _subscriptions_free_list = std::move(h._subscriptions_free_list);

            update_connections();

            return *this;
        }

        size_t connections_count() const noexcept {
            return _connections.size() - _connections_free_list.size();
        }

        template <signal::source_node S>
        connection_id<typename S::signal_type> get_connection(const S & source) const noexcept {
            if (auto it = find_connection(_connections, &source); it != _connections.end()) {
                return connection_id<typename S::signal_type>{it->id};
            }

            return {};
        }

        template <signal::source_node S>
        bool has_connection(const S & source) const noexcept {
            return find_connection(_connections, &source) != _connections.end();
        }

        template <signal::source_node S>
            requires (!std::is_const_v<S>)
        connection_id<typename S::signal_type> connect(S & source) noexcept {
            if (auto it = find_connection(_connections, &source); it != _connections.end()) {
                return connection_id<typename S::signal_type>{it->id};
            }

            u32 id = allocate_connection();

            auto & c = _connections[id];
            c.destination = this;
            c.bind(source, *this);

            return id;
        }

        template <signal::source_node S>
        bool disconnect(S & source) noexcept {
            if (auto it = find_connection(_connections, &source); it != _connections.end()) {
                it->reconnect(nullptr, &*it, nullptr, nullptr);
                return true;
            }

            return false;
        }

        void disconnect(u32 id) noexcept {
            if (auto & c = _connections[id]; c.reconnect != nullptr) {
                c.reconnect(nullptr, &c, nullptr, nullptr);
            }
        }

        template <class F>
        subscription_id<F> subscribe(connection_id<F> conn_id, move_only_function<identity_t<F>> && handler) noexcept {
            static_assert(sizeof(subscription<F>) == sizeof(subscription_storage_type) && alignof(subscription<F>) == alignof(subscription_storage_type));

            auto sub_id = allocate_subscription();
            auto s = std::construct_at(get_subscription<F>(_subscriptions[sub_id]), sub_id, conn_id, std::move(handler));
            _connections[conn_id].subscriptions.push_back(s);

            return subscription_id<F>{s->id};
        }

        template <signal::source_node S>
        auto subscribe(S & source, move_only_function<typename S::signal_type> && handler) noexcept {
            auto conn_id = connect(source);
            return subscribe(conn_id, std::move(handler));
        }

        void unsubscribe(u32 id) noexcept {
            if (auto * s = get_basic_subscription(_subscriptions[id]); s->destroy) {
                remove_subscription(s);
                destroy_subscription(s);
            }
        }

        template <class ... A>
        void operator()(signal::transmission<void(A...), hub> & transmission, identity_t<A> ... args) {
            auto & c = transmission.connection;

            for (auto s : c.subscriptions) {
                auto & dest = *static_cast<subscription<void(A...)> *>(s);
                dest.handler(std::forward<A>(args)...);
            }
        }

        template <class Callback, class R, class ... A>
            requires(std::invocable<Callback, R>)
        void operator()(signal::transmission<R(A...), hub> & transmission, Callback && callback, identity_t<A> ... args) {
            auto & c = transmission.connection;

            for (auto s : c.subscriptions) {
                auto & dest = *static_cast<subscription<R(A...)> *>(s);
                std::forward<Callback>(callback)(dest.handler(std::forward<A>(args)...));
            }
        }

        u32 plug(connection_type && c) {
            if (c.destination != this) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Trying to plug unrelated connection"));
            }

            if (!c.id.valid()) {
                c.id = allocate_connection();
            }

            u32 id = c.id;

            _connections[id] = std::move(c);

            return id;
        }

        connection_type unplug(u32 id) {
            return std::move(_connections[id]);
        }

    protected:
        friend struct connection;

        static auto find_connection(auto & list, const void * source) noexcept {
            return std::find_if(list.begin(), list.end(), [source](auto & c) { return c.source == source; });
        }

        void update_connections() {
            for (auto & c : _connections) {
                if (c.reconnect) {
                    c.reconnect(nullptr, &c, c.source, this);
                }
            }
        }

        basic_connection_id allocate_connection() noexcept {
            if (_connections_free_list.empty()) {
                auto id = static_cast<u32>(_connections.size());
                _connections.emplace_back(id);
                return id;
            }

            auto id = std::move(_connections_free_list.back());
            _connections_free_list.pop_back();

            return id;
        }

        void cleanup_connection(connection_type & c) noexcept {
            if (!c.id.valid()) {
                return;
            }

            c.destination = nullptr;

            for (auto s : c.subscriptions) {
                destroy_subscription(s);
            }

            c.subscriptions.clear();
            _connections_free_list.push_back(std::move(c.id));
        }

//---------------------------------------------------------------------------

        using subscription_storage_type = typename std::aligned_storage<sizeof(subscription<void()>), alignof(subscription<void()>)>::type;

        template <class F>
        subscription<F> * get_subscription(subscription_storage_type & storage) noexcept {
            return static_cast<subscription<F> *>(static_cast<void *>(&storage));
        }

        basic_subscription * get_basic_subscription(subscription_storage_type & storage) noexcept {
            return static_cast<basic_subscription *>(static_cast<void *>(&storage));
        }

        basic_subscription_id allocate_subscription() noexcept {
            if (_subscriptions_free_list.empty()) {
                _subscriptions.emplace_back();
                return static_cast<u32>(_subscriptions.size() - 1);
            }

            auto id = std::move(_subscriptions_free_list.back());
            _subscriptions_free_list.pop_back();

            return id;
        }

        void remove_subscription(basic_subscription * sub) noexcept {
            auto & c = _connections[sub->connection_id];

            auto it = std::find(c.subscriptions.begin(), c.subscriptions.end(), sub);

            if (it != c.subscriptions.end()) {
                c.subscriptions.erase(it);
            }
        }

        void destroy_subscription(basic_subscription * sub) noexcept {
            auto id = std::move(sub->id);

            sub->destroy(sub);
            sub->destroy = nullptr;

            _subscriptions_free_list.push_back(std::move(id));
        }

        std::pmr::vector<connection_type> _connections;
        boost::container::stable_vector<subscription_storage_type, std::pmr::polymorphic_allocator<subscription_storage_type>> _subscriptions;

        std::pmr::vector<basic_connection_id> _connections_free_list;
        std::pmr::vector<basic_subscription_id> _subscriptions_free_list;
    };

    inline signal::hub global_hub;
}
