//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/basic_source.h>

//---------------------------------------------------------------------------

namespace asd::signal
{
    template <class Owner, class F, class Alloc = std::pmr::polymorphic_allocator<connection *>>
    class member_source
    {
        static_assert(meta::always_false<F>, "member_source requires its template argument to represent the handler function signature (e.g. member_source<owner, void(int)>)");
    };

    template <class Owner, class R, class Alloc, class ... Arguments>
    class member_source<Owner, R(Arguments...), Alloc> : public basic_source<R(Arguments...), Alloc>
    {
    public:
        using base = basic_source<R(Arguments...), Alloc>;

        using base::base;
        using base::operator =;

    private:
        friend Owner;

        template <class ... A>
            requires (... && std::convertible_to<A &&, Arguments>)
        void operator()(A && ... args) requires (std::same_as<R, void>) {
            for (auto c : this->_connections) {
                this->invoke(*c, base::transmission_type(std::forward<A>(args) ...));
            }
        }

        template <class F, class ... A>
            requires (!std::same_as<R, void> && std::invocable<F, R> && (... && std::convertible_to<A &&, Arguments>))
        void operator()(F && result_handler, A && ... args) {
            for (auto c : this->_connections) {
                this->invoke(*c, base::transmission_type(std::forward<F>(result_handler), std::forward<A>(args) ...));
            }
        }

        template <class OutputIterator, class ... A>
            requires (result_output_iterator<OutputIterator, R> && (... && std::convertible_to<A &&, Arguments>))
        void operator()(OutputIterator it, A && ... args) {
            for (auto c : this->_connections) {
                this->invoke(*c, base::transmission_type(result_inserter<OutputIterator, R>(it), std::forward<A>(args) ...));
            }
        }
    };
}
