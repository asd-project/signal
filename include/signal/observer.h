//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/connection.h>

#include <boost/assert.hpp>

//---------------------------------------------------------------------------

namespace asd::signal
{
    template <class F>
    class observer
    {
        static_assert(meta::always_false<F>, "observer requires its template argument to represent the handler move_only_function signature (e.g. observer<void(int)>)");
    };

    /**
     * Signal destination with a single possible connection
     */
    template <class R, class ... Arguments>
    class observer<R(Arguments...)>
    {
    public:
        using connection_type = signal::connection;

        observer() noexcept = default;

        observer(observer && o) noexcept :
            _handler(std::move(o._handler)),
            _connection(std::move(o._connection))
        {
            if (_connection.reconnect) {
                _connection.destination = this;
            }
        }

        template<class S>
        observer(S & source, move_only_function<R(Arguments...)> handler) noexcept :
            _handler(std::move(handler)),
            _connection(source, *this)
        {}

        observer(move_only_function<R(Arguments...)> handler) noexcept :
            _handler(std::move(handler))
        {}

        observer & operator = (observer && o) noexcept {
            _handler = std::move(o._handler);
            _connection = std::move(o._connection);

            if (_connection.reconnect) {
                _connection.destination = this;
            }

            return *this;
        }

        template <signal::source_node S>
        bool has_connection(const S & source) const noexcept {
            return _connection.source == &source;
        }

        template <signal::source_node S>
        auto connect(S & source) {
            _connection = connection_type(source, *this);
            return &_connection;
        }

        template <signal::source_node S>
        bool disconnect(S & source) {
            if (_connection.source != &source) {
                return false;
            }

            _connection = {};
            return true;
        }

        void subscribe(move_only_function<R(Arguments...)> handler) {
            _handler = std::move(handler);
        }

        template <signal::source_node S>
        void subscribe(S & source, move_only_function<R(Arguments...)> handler) {
            _connection = connection_type(source, *this);
            _handler = std::move(handler);
        }

        void unsubscribe() {
            _handler = {};
        }

        void clear() {
            _connection = {};
            unsubscribe();
        }

        template <class ... A>
            requires(std::same_as<R, void> && std::invocable<move_only_function<R(Arguments...)>, A...>)
        void operator()(signal::transmission<void(A...), observer> &, identity_t<A> ... args) {
            BOOST_ASSERT_MSG(_handler, "Trying to call observer without bound handler");

            if (_handler) {
                _handler(std::forward<A>(args)...);
            }
        }

        template <class Callback, class ... A>
            requires(std::invocable<Callback, R> && std::invocable<move_only_function<R(Arguments...)>, A...>)
        void operator()(signal::transmission<R(A...), observer> &, Callback && callback, identity_t<A> ... args) {
            BOOST_ASSERT_MSG(_handler, "Trying to call observer without bound handler");

            if (_handler) {
                std::forward<Callback>(callback)(_handler(std::forward<A>(args)...));
            }
        }

    private:
        friend struct connection;

        void cleanup_connection(connection_type & c) noexcept {
            if (&_connection != &c) {
                return;
            }

            c.destination = nullptr;
            _handler = {};
        }

        move_only_function<R(Arguments...)> _handler;
        connection_type _connection;
    };
}
