//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <memory_resource>

#include <signal/connection.h>

//---------------------------------------------------------------------------

namespace asd::signal
{
    template <class F, class Alloc = std::pmr::polymorphic_allocator<connection *>>
    class basic_source
    {
        static_assert(meta::always_false<F>, "basic_source requires the first template argument to represent the handler function signature (e.g. basic_source<void(int)>)");
    };

    template <class R, class Alloc, class ... Arguments>
    class basic_source<R(Arguments...), Alloc>
    {
    public:
        using traits = signal_traits<R, Arguments...>;
        using signal_type = typename traits::signal_type;
        using arguments_type = typename traits::arguments_type;
        using transmission_type = typename traits::transmission_type;

        using allocator_type = Alloc;

        basic_source(const allocator_type & alloc = {}) noexcept :
            _connections(alloc) {}

        basic_source(basic_source && s, const allocator_type & alloc = {}) noexcept :
            _connections(std::move(s._connections), alloc)
        {
            for (auto & c : _connections) {
                c->source = this;
            }
        }

        ~basic_source() {
            cleanup_connections();
        }

        basic_source & operator = (basic_source && s) noexcept {
            cleanup_connections();
            _connections = std::move(s._connections);

            for (auto & c : _connections) {
                c->source = this;
            }

            return *this;
        }

        size_t connections_count() const noexcept {
            return _connections.size();
        }

        template <signal::owning_node T>
        bool has_connection(T & destination) const noexcept {
            return destination.has_connection(*this);
        }

        template <signal::owning_node T>
        auto connect(T & destination) noexcept {
            return destination.connect(*this);
        }

        template <signal::owning_node T>
        bool disconnect(T & destination) noexcept {
            return destination.disconnect(*this);
        }

        void clear() noexcept {
            cleanup_connections();
            _connections.clear();
        }

    protected:
        friend struct connection;

        void set_connection(connection & c, connection * previous) noexcept {
            if (previous) {
                if (auto it = std::find(_connections.begin(), _connections.end(), previous); it != _connections.end()) {
                    c.source = this;
                    *it = &c;
                }

                return;
            }

            c.source = this;
            _connections.push_back(&c);
        }

        void reset_connection(connection & c) noexcept {
            if (auto it = std::find(_connections.begin(), _connections.end(), &c); it != _connections.end()) {
                (*it)->source = nullptr;
                _connections.erase(it);
            }
        }

        void invoke(connection & c, transmission_type && transmission) {
            c.send(&c, &transmission);
        }

        void cleanup_connections() {
            for (auto c : _connections) {
                c->source = nullptr;
                c->reconnect(nullptr, c, nullptr, nullptr);
            }
        }

        std::vector<connection *, allocator_type> _connections;
    };
}
