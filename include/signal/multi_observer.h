//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/connection.h>

#include <boost/assert.hpp>

//---------------------------------------------------------------------------

namespace asd::signal
{
    template <class F>
    class multi_observer
    {
        static_assert(meta::always_false<F>, "multi_observer requires its template argument to represent the handler function signature (e.g. multi_observer<void(int)>)");
    };

    /**
     * Signal destination with multiple possible connections but a single handler
     */
    template <class R, class ... Arguments>
    class multi_observer<R(Arguments...)>
    {
    public:
        using connection_type = signal::connection;
        using allocator_type = std::pmr::polymorphic_allocator<connection_type>;

        multi_observer(const allocator_type & alloc = {}) noexcept :
            _connections(alloc),
            _connections_free_list(alloc)
        {}

        multi_observer(move_only_function<R(Arguments...)> handler = {}, const allocator_type & alloc = {}) noexcept :
            _connections(alloc),
            _connections_free_list(alloc),
            _handler(std::move(handler))
        {}

        multi_observer(multi_observer && o, const allocator_type & alloc = {}) noexcept :
            _connections(std::move(o._connections), alloc),
            _connections_free_list(std::move(o._connections_free_list), alloc),
            _handler(std::move(o._handler))
        {
            update_connections();
        }

        multi_observer & operator = (multi_observer && o) noexcept {
            _connections = std::move(o._connections);
            _connections_free_list = std::move(o._connections_free_list);
            _handler = std::move(o._handler);

            update_connections();

            return *this;
        }

        template <signal::source_node S>
        bool has_connection(const S & source) const noexcept {
            return find_connection(_connections, &source) != _connections.end();
        }

        template <signal::source_node S>
        auto connect(S & source) {
            if (auto it = find_connection(_connections, &source); it != _connections.end()) {
                return static_cast<u32>(std::distance(_connections.begin(), it));
            }

            u32 id = allocate_connection();

            auto & c = _connections[id];
            c.destination = this;
            c.bind(source, *this);

            return id;
        }

        template <signal::source_node S>
        bool disconnect(S & source) noexcept {
            if (auto it = find_connection(_connections, &source); it != _connections.end()) {
                it->reconnect(nullptr, &*it, nullptr, nullptr);
                return true;
            }

            return false;
        }

        void disconnect(u32 id) noexcept {
            if (auto & c = _connections[id]; c.reconnect != nullptr) {
                c.reconnect(nullptr, &c, nullptr, nullptr);
            }
        }

        void subscribe(move_only_function<R(Arguments...)> handler) {
            _handler = std::move(handler);
        }

        void unsubscribe() {
            _handler = {};
        }

        void clear() {
            _connections.clear();
            _connections_free_list.clear();
            unsubscribe();
        }

        template <class ... A>
            requires(std::same_as<R, void> && std::invocable<function<R(Arguments...)>, A...>)
        void operator()(signal::transmission<void(A...), multi_observer> &, identity_t<A> ... args) {
            BOOST_ASSERT_MSG(_handler, "Trying to call multi_observer without bound handler");

            if (_handler) {
                _handler(std::forward<A>(args)...);
            }
        }

        template <class Callback, class ... A>
            requires(std::invocable<Callback, R> && std::invocable<function<R(Arguments...)>, A...>)
        void operator()(signal::transmission<R(A...), multi_observer> &, Callback && callback, identity_t<A> ... args) {
            BOOST_ASSERT_MSG(_handler, "Trying to call multi_observer without bound handler");

            if (_handler) {
                std::forward<Callback>(callback)(_handler(std::forward<A>(args)...));
            }
        }

    private:
        friend struct connection;

        static auto find_connection(auto & list, const void * source) noexcept {
            return std::find_if(list.begin(), list.end(), [source](auto & c) { return c.source == source; });
        }

        void update_connections() {
            for (auto & c : _connections) {
                if (c.reconnect != nullptr) {
                    c.reconnect(nullptr, &c, c.source, this);
                }
            }
        }

        u32 allocate_connection() noexcept {
            if (_connections_free_list.empty()) {
                auto id = static_cast<u32>(_connections.size());
                _connections.emplace_back();
                return id;
            }

            auto id = std::move(_connections_free_list.back());
            _connections_free_list.pop_back();

            return id;
        }

        void cleanup_connection(connection_type & c) noexcept {
            auto ptrdiff = &c - _connections.data();

            if (ptrdiff < 0 || ptrdiff >= _connections.size()) {
                return;
            }

            c.destination = nullptr;

            _connections_free_list.push_back(static_cast<u32>(ptrdiff));
        }

        std::pmr::vector<connection_type> _connections;
        std::pmr::vector<u32> _connections_free_list;

        move_only_function<R(Arguments...)> _handler;
    };
}
