//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <memory_resource>

#include <meta/unique_value.h>

#include <signal/connection.h>

#include <boost/assert.hpp>

//---------------------------------------------------------------------------

namespace asd::signal
{
    template <class F>
    class exclusive_source
    {
        static_assert(meta::always_false<F>, "exclusive_source requires the first template argument to represent the handler function signature (e.g. exclusive_source<void(int)>)");
    };

    template <class R, class ... Arguments>
    class exclusive_source<R(Arguments...)>
    {
    public:
        using traits = signal_traits<R, Arguments...>;
        using signal_type = typename traits::signal_type;
        using arguments_type = typename traits::arguments_type;
        using transmission_type = typename traits::transmission_type;

        exclusive_source() noexcept = default;

        exclusive_source(exclusive_source && s) noexcept :
            _connection(std::move(s._connection))
        {
            if (_connection) {
                _connection->source = this;
            }
        }

        template <signal::owning_node T>
        exclusive_source(T & destination) noexcept {
            destination.connect(*this);
        }

        ~exclusive_source() {
            cleanup_connection();
        }

        exclusive_source & operator = (exclusive_source && s) noexcept {
            cleanup_connection();
            _connection = std::move(s._connection);

            if (_connection) {
                _connection->source = this;
            }

            return *this;
        }

        size_t connections_count() const noexcept {
            return _connection ? 1 : 0;
        }

        template <signal::owning_node T>
        bool has_connection(T & destination) const noexcept {
            return destination.has_connection(*this);
        }

        template <signal::owning_node T>
        auto connect(T & destination) noexcept {
            return destination.connect(*this);
        }

        template <signal::owning_node T>
        bool disconnect(T & destination) noexcept {
            return destination.disconnect(*this);
        }

        void clear() noexcept {
            cleanup_connection();
        }

        template <class ... A>
            requires (... && std::convertible_to<A &&, Arguments>)
        void operator()(A && ... args) requires (std::same_as<R, void>) {
            if (_connection) {
                this->invoke(transmission_type(std::forward<A>(args) ...));
            }
        }

        template <class F, class ... A>
            requires (!std::same_as<R, void> && std::invocable<F, R> && (... && std::convertible_to<A &&, Arguments>))
        void operator()(F && result_handler, A && ... args) {
            if (_connection) {
                this->invoke(transmission_type(std::forward<F>(result_handler), std::forward<A>(args) ...));
            }
        }

        template <class OutputIterator, class ... A>
            requires (result_output_iterator<OutputIterator, R> && (... && std::convertible_to<A &&, Arguments>))
        void operator()(OutputIterator it, A && ... args) {
            if (_connection) {
                this->invoke(transmission_type(result_inserter<OutputIterator, R>(it), std::forward<A>(args) ...));
            }
        }

    private:
        friend struct connection;

        void set_connection(connection & c, connection * previous) noexcept {
            cleanup_connection();

            c.source = this;
            _connection = &c;
        }

        void reset_connection(connection & c) noexcept {
            BOOST_ASSERT_MSG(&c == _connection, "Trying to reset unknown connection");

            c.source = nullptr;
            _connection = nullptr;
        }

        void invoke(transmission_type && transmission) {
            _connection->send(_connection, &transmission);
        }

        void cleanup_connection() {
            if (!_connection) {
                return;
            }

            if (_connection->reconnect) {
                _connection->source = nullptr;
                _connection->reconnect(nullptr, _connection, nullptr, nullptr);
            }

            _connection = nullptr;
        }

        meta::unique_value<signal::connection *> _connection;
    };
}
