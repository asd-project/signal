//---------------------------------------------------------------------------

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <launch/main.h>
#include <signal/signal.h>

//---------------------------------------------------------------------------

namespace asd::app
{
    TEST_CASE("void signal", "[asd::signal]") {
        int counter = 0;

        signal::source<void()> s;

        {
            signal::hub hub;

            hub.subscribe(s, [&]() {
                ++counter;
            });

            REQUIRE(s.has_connection(hub));

            s();
            REQUIRE(counter == 1);

            hub.subscribe(s, [&]() {
                ++counter;
            });

            REQUIRE(s.connections_count() == 1); // there is still only one connection

            s();
            REQUIRE(counter == 3); // counter was incremented twice

            signal::hub another_hub = std::move(hub);
            REQUIRE(!s.has_connection(hub));
            REQUIRE(s.has_connection(another_hub));

            s();
            REQUIRE(counter == 5); // counter was incremented twice again
        }

        s();
        REQUIRE(counter == 5); // no counter increments without hub connection
        REQUIRE(s.connections_count() == 0);

        {
            signal::hub hub;

            hub.subscribe(s, [&]() {
                ++counter;
            });

            REQUIRE(s.has_connection(hub));

            s();
            REQUIRE(counter == 6);

            signal::source s2(std::move(s));

            REQUIRE(!s.has_connection(hub));
            REQUIRE(s2.has_connection(hub));

            s();
            REQUIRE(counter == 6);

            s2();
            REQUIRE(counter == 7);

            signal::source<void()> s3;
            s3 = std::move(s2);

            REQUIRE(!s2.has_connection(hub));
            REQUIRE(s3.has_connection(hub));

            s2();
            REQUIRE(counter == 7);

            s3();
            REQUIRE(counter == 8);
        }
    }

    TEST_CASE("string signal", "[asd::signal]") {
        signal::source<void(const std::string &)> s;
        signal::hub hub;

        std::string received_message;

        hub.subscribe(s, [&](const std::string & message) {
            received_message = message;
        });

        s("OK");
        REQUIRE(received_message == "OK");
    }

    TEST_CASE("disconnect signal", "[asd::signal]") {
        signal::source<void(const std::string &)> s;
        signal::hub hub;

        std::string received_message;

        hub.subscribe(s, [&](const std::string & message) {
            received_message = message;
        });

        s("OK");
        REQUIRE(received_message == "OK");

        s.disconnect(hub);
        received_message.clear();

        s("NOT OK");
        REQUIRE(received_message.empty());
        REQUIRE(s.connections_count() == 0);
    }

    TEST_CASE("unsubscribe signal", "[asd::signal]") {
        signal::source<void(const std::string &)> s;
        signal::hub hub;

        std::string received_message;

        auto id = hub.subscribe(s, [&](const std::string & message) {
            received_message = message;
        });

        s("OK");
        REQUIRE(received_message == "OK");

        hub.unsubscribe(id);
        received_message.clear();

        s("NOT OK");
        REQUIRE(received_message.empty());
        REQUIRE(s.connections_count() == 1); // connection still exists

        auto connection_id = hub.get_connection(s);
        REQUIRE(connection_id.valid());

        hub.subscribe(connection_id, [&](const std::string & message) {
            received_message = message;
        });

        s("NICE");
        REQUIRE(received_message == "NICE");
    }

    TEST_CASE("multi-argument signal", "[asd::signal]") {
        signal::source<void(int, float, const std::string &)> s;
        signal::hub hub;

        int received_int;
        float received_float;
        std::string received_string;

        hub.subscribe(s, [&](int i, float f, const std::string & s) {
            received_int = i;
            received_float = f;
            received_string = s;
        });

        s(100, 300.0f, "OK");
        REQUIRE(received_int == 100);
        REQUIRE(received_float == 300.0f);
        REQUIRE(received_string == "OK");
    }

    TEST_CASE("multiple signals", "[asd::signal]") {
        signal::hub hub;
        signal::source<void(int, float, const std::string &)> s1;
        signal::source<void(float, const std::string &)> s2;

        int received_int;
        float received_float;
        std::string received_string;

        hub.subscribe(s1, [&](int i, float f, const std::string & s) {
            received_int = i;
            received_float = f;
            received_string = s;
        });

        hub.subscribe(s2, [&](int i, const std::string & s) { // implicit conversion
            received_int += i;
            received_string.append(" ").append(s);
        });

        s1(100, 300.0f, "EVERYTHING");
        s2(100, "IS OK");
        REQUIRE(received_int == 200);
        REQUIRE(received_float == 300.0f);
        REQUIRE(received_string == "EVERYTHING IS OK");
    }

    TEST_CASE("manual reconnection", "[asd::signal]") {
        signal::hub hub;
        signal::source<void(int, float, const std::string &)> s1;
        signal::source<void(float, const std::string &)> s2;

        signal::hub_connection conn(s1, hub);
        REQUIRE(s1.connections_count() == 1);
        REQUIRE(!hub.has_connection(s1));       // hub doesn't really know about external connection

        conn = signal::hub_connection(s2, hub);     // connection is overwritten
        REQUIRE(s1.connections_count() == 0);
        REQUIRE(s2.connections_count() == 1);

        auto id = hub.plug(std::move(conn));
        REQUIRE(s2.connections_count() == 1);   // source is still connected
        REQUIRE(hub.has_connection(s2));        // now hub is connected too

        conn = hub.unplug(id);
        REQUIRE(s2.connections_count() == 1);   // source is still connected
        REQUIRE(!hub.has_connection(s2));       // hub is disconnected now
    }

    TEST_CASE("invalid manual reconnection", "[asd::signal]") {
        signal::hub hub1;
        signal::hub hub2;
        signal::source<void(int, float, const std::string &)> s1;
        signal::source<void(float, const std::string &)> s2;

        signal::hub_connection conn(s1, hub1);
        REQUIRE(s1.connections_count() == 1);
        REQUIRE(!hub1.has_connection(s1));          // hub doesn't really know about external connection

        conn = signal::hub_connection(s2, hub2);    // connection is overwritten
        REQUIRE(s1.connections_count() == 0);
        REQUIRE(s2.connections_count() == 1);

        REQUIRE_THROWS_AS(hub1.plug(std::move(conn)), std::runtime_error);   // try to plug unrelated connection
        REQUIRE(s2.connections_count() == 1);       // source is still connected
        REQUIRE(!hub1.has_connection(s2));          // hub1 is still not connected

        auto id = hub2.plug(std::move(conn));
        REQUIRE(id != uint32(-1));
        REQUIRE(s2.connections_count() == 1);       // source is still connected
        REQUIRE(hub2.has_connection(s2));           // hub2 is connected
    }

    TEST_CASE("accumulating signals", "[asd::signal]") {
        signal::hub hub;
        signal::source<int()> s;

        hub.subscribe(s, []() {
            return 1;
        });

        int accumulator = 0;

        auto intercept_result = [&](int result) {
            accumulator += result;
        };

        s(intercept_result);
        REQUIRE(accumulator == 1);
        s(intercept_result);
        REQUIRE(accumulator == 2);

        std::vector<int> v;
        s(std::back_inserter(v));

        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == 1);
    }

    TEST_CASE("complex accumulating signals", "[asd::signal]") {
        signal::hub hub;
        signal::source<std::string(const std::string & s)> s;

        hub.subscribe(s, [](const std::string & s) {
            return s + " from Alice";
        });

        hub.subscribe(s, [](const std::string & s) {
            return s + " from Bob";
        });

        std::vector<std::string> v;
        s(std::back_inserter(v), "Hello");

        REQUIRE(v.size() == 2);
        REQUIRE(v[0] == "Hello from Alice");
        REQUIRE(v[1] == "Hello from Bob");
    }

    TEST_CASE("basic observer", "[asd::signal]") {
        std::string received_message;

        signal::source<void(const std::string & s)> s;
        signal::observer<void(const std::string & s)> o;

        o.subscribe(s, [&](const std::string & msg) {
            received_message = msg;
        });

        s("OK");
        REQUIRE(received_message == "OK");

        s.disconnect(o);

        s("NOT OK");
        REQUIRE(received_message == "OK");

        {
            signal::observer<void(const std::string & s)> scoped(s, [&](const std::string & msg) {
                received_message = msg;
            });
        }

        s("NOT OK");
        REQUIRE(received_message == "OK");

        {
            o.subscribe(s, [&](const std::string & msg) {
                received_message = msg;
            });

            signal::observer<void(const std::string & s)> o2(std::move(o));

            REQUIRE(!s.has_connection(o));
            REQUIRE(s.has_connection(o2));

            s("Call move-constucted observer");
            REQUIRE(received_message == "Call move-constucted observer");

            signal::observer<void(const std::string & s)> o3;
            o3 = std::move(o2);

            REQUIRE(!s.has_connection(o2));
            REQUIRE(s.has_connection(o3));

            s("Call move-assigned observer");
            REQUIRE(received_message == "Call move-assigned observer");

            received_message.clear();
        }

        s("NOT OK");
        REQUIRE(received_message.empty());
    }

    TEST_CASE("accumulating signal with observer", "[asd::signal]") {
        signal::source<std::string(const std::string & s)> s;
        signal::observer<std::string(const std::string & s)> o(s, [&](const std::string & msg) {
            return "EVERYTHING IS " + msg;
        });

        std::string received_message;
        s(&received_message, "OK");
        REQUIRE(received_message == "EVERYTHING IS OK");

        s.disconnect(o);
        s(&received_message, "NOT OK");
        REQUIRE(received_message == "EVERYTHING IS OK");
    }

    TEST_CASE("exclusive source", "[asd::signal]") {
        std::string received_message;
        signal::observer<void(const std::string & s)> recording_observer{[&](const std::string & s) {
            received_message = s;
        }};

        signal::observer<void(const std::string & s)> null_observer{[&](const std::string &) {
            // ignore
        }};

        signal::exclusive_source<void(const std::string & s)> s;

        s.connect(recording_observer);
        s("EVERYTHING IS OK");
        REQUIRE(received_message == "EVERYTHING IS OK");

        s.connect(null_observer);
        s("NOT OK");
        REQUIRE(received_message == "EVERYTHING IS OK");
    }

    TEST_CASE("multi-observer", "[asd::signal]") {
        struct observer_state
        {
            void operator()(const std::string & msg) {
                if (!received_message.empty()) {
                    received_message += " " + msg;
                } else {
                    received_message += msg;
                }
            }

            std::string received_message;
        } state;

        signal::multi_observer<void(const std::string & s)> o{std::ref(state)};

        signal::exclusive_source<void(const std::string & s)> s1{o};
        signal::exclusive_source<void(const std::string & s)> s2{o};
        signal::exclusive_source<void(const std::string & s)> s3{o};

        s1("EVERYTHING");
        s2("IS");
        s3("OK");

        REQUIRE(state.received_message == "EVERYTHING IS OK");
    }

    static ::asd::entrance open([](int argc, char * argv[]) {
        Catch::Session session;

        if (int return_code = session.applyCommandLine(argc, argv); return_code != 0) {
            return return_code;
        }

        return session.run();
    });
}

//---------------------------------------------------------------------------
