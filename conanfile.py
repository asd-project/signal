
from conans import ConanFile

project_name = "signal"


class SignalConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Lightweight and extensible signal-slot implementation"
    topics = ("asd", project_name)
    generators = "cmake"
    settings = "os", "compiler"
    exports_sources = "include*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.flow/0.0.1@asd/testing",
        "function_ref/1.0.0@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")
